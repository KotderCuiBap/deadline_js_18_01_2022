function soTBC() {
  var num1 = document.getElementById("txt_1st_num").value * 1;
  var num2 = document.getElementById("txt_2nd_num").value * 1;
  var num3 = document.getElementById("txt_3rd_num").value * 1;
  var num4 = document.getElementById("txt_4th_num").value * 1;
  var num5 = document.getElementById("txt_5th_num").value * 1;

  var soTBC = (num1 + num2 + num3 + num4 + num5) / 5;
  document.getElementById(
    "result"
  ).innerText = `Số trung bình cộng là ${soTBC} `;
}

/** Mô hình 3 khối bài tính số tbc
 *  Bước 1:
 *     Input:Nhập num1,num2,num3,num4,num5 vào các ô input
 *  Bước 2:
 *     Logic:Để tính số tbc thì ta lấy num1+num2+num3+num4+num5 rồi sau đó chia cho 5
 * Bước 3:
 *     Output:Ghi kết quả(soTBC) vào ô result
 */
