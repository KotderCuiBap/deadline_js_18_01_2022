function tongLuong() {
  var salary_per_day = document.getElementById("txt_salary_per_day").value * 1;
  var working_day = document.getElementById("txt_working_day").value * 1;

  var tongTien = salary_per_day * working_day;
  document.getElementById("result").innerText = `Tổng lương là ${tongTien} $ `;
}
/** Mô hình 3 khối bài tính tiền lương
 *  Bước 1:
 *     Input:Nhập salary per day và working day vào input
 *  Bước 2:
 *     Logic:Để tính tiền lương thì ta lấy salary per day(lương/ngày) * working day(số ngày làm việc)
 * Bước 3:
 *     Output:Ghi kết quả(tongTien) vào ô result
 */
