function kiSo() {
  var number = document.getElementById("txt_number").value * 1;
  var soHangDonVi = number % 10;
  var soHangChuc = number / 10;
  var kiSo = soHangChuc + soHangDonVi;
  kiSo = Math.floor(kiSo);
  document.getElementById("result").innerText = `Kí số bằng ${kiSo}`;
}
/**
 * Bước 1 :Nhập input:Nhập số cần tính kí số vào ô input
 * Bước 2 :Logic:
 *   Tính số hàng đơn vị
 *   Tính số hàng chục
 * Sau đó cộng hai số lại với nhau và dùng hàm Math.floor() để loại bỏ phần thập phân
 * Cập nhật lại giá trị của biến kiSo
 * Bước 3:Output:
 *   Xuất kết quả ra ô result
 */
