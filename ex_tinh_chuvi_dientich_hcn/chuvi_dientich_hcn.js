var chieuDai = document.getElementById("txt_chieu_dai").value * 1;
var chieuRong = document.getElementById("txt_chieu_rong").value * 1;
function chuVi() {
  var chuVi = (chieuDai + chieuRong) * 2;
  document.getElementById("result_chuvi").innerText = `Chu vi là ${chuVi}`;
}

function dienTich() {
  var dienTich = chieuDai * chieuRong;
  document.getElementById(
    "result_dientich"
  ).innerText = `Diện tích là ${dienTich}`;
}
/**
 * Bước 1 :Nhập input:Nhập chu vi và chiều dài hcn vào ô input
 * Bước 2 :Logic:
 *   Tính chu vi:(chiều dài+ chiều rộng)*2
 *   Tính diện tích :(chiều dài* chiều rộng)
 * Bước 3:Output:
 *   Xuất kết quả ra ô result_chuvi và result_dientich
 *
 */
